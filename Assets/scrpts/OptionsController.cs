﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsController : MonoBehaviour {
    [Range(1, 500)] public float sensitivity ;
    public Dropdown microphone;
	public Slider sensitivitySlider, thresholdSlider;
    public GameObject settingsPanel;

    

	// Use this for initialization
	void Start () {
		microphone.value = PlayerPrefsManager.GetMicrophone();
		sensitivitySlider.value = PlayerPrefsManager.GetSensitivity ();
		thresholdSlider.value = PlayerPrefsManager.GetThreshold ();
	}

	public void SaveAndExit (){
		PlayerPrefsManager.SetMicrophone (microphone.value);
		PlayerPrefsManager.SetSensitivity (sensitivitySlider.value);
		PlayerPrefsManager.SetThreshold (thresholdSlider.value);
        sensitivity = PlayerPrefsManager.GetSensitivity();
        Debug.Log("saved");
    }

	public void SetDefaults(){
		microphone.value = 0;
		sensitivitySlider.value = 100f;
		thresholdSlider.value = 0.001f;
        Debug.Log("null");
	}




}
