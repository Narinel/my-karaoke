﻿using UnityEngine;
using UnityEngine.UI; //for accessing Sliders and Dropdown
using System.Collections.Generic; // So we can use List<>
using System;

[RequireComponent(typeof(AudioSource))]
public class SoundSC : MonoBehaviour
{
    public float loudness = 0;
    public float sensetivity;
    private AudioSource audioSource;
    public GameObject playera;
    public List<string> options;
    public Dropdown drop;
    public string microphone;
    private void Start()
    {



        drop.value = PlayerPrefsManager.GetMicrophone();
        foreach (string device in Microphone.devices)
        {
            if (microphone == null)
            {
                //set default mic to first mic found.
                microphone = device;
            }
            options.Add(device);
        }
            microphone = options[PlayerPrefsManager.GetMicrophone()];
        drop.AddOptions(options);
        

        AudioSource aud = GetComponent<AudioSource>();
            aud.clip = Microphone.Start(null, true, 10, 44100);
            aud.loop = true;
            while (!(Microphone.GetPosition(null) > 0)) ;
            aud.Play();


        
    }

    void OnGUI()
    {
        GUI.Label(new Rect(700, 320, 100, 20), loudness.ToString());
    }
    void Update()
    {
        //loudnesss = loudness.ToString;
        sensetivity = PlayerPrefsManager.GetSensitivity();
        loudness = GetAveragedVolume() * sensetivity;
        if (loudness > 10)
            playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 30);
        if (loudness > 23)
            playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 75);
        if (loudness > 35)
            playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 100);
        if (loudness > 45)
            playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 150);
        if (loudness > 55)
            playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 200);
        if (loudness > 65)
            playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 300);
    }

    public float GetAveragedVolume()  // метод получения громкости
    {
        AudioSource aud = GetComponent<AudioSource>();
        float[] data = new float[256];
        float a = 0;
        aud.GetOutputData(data, 0); 
        foreach (float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a / 256;
    }
    
}