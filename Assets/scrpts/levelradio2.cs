﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelradio2 : MonoBehaviour {

    public AudioSource hudio;
    WWW www;
    public string path;

    private void Start()
    {
        path = loadmylevel2.vs;
        lastg();
    }
    public void lastg()
    {
        www = new WWW("file://" + path);
        hudio.clip = www.GetAudioClip(false, true, AudioType.OGGVORBIS);
        hudio.Play();
    }
}
