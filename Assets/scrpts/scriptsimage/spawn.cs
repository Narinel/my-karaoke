﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class spawn : MonoBehaviour {
    public int spawnnum;
    public string pathimg;
    public string another;
    public GameObject image2;
    UnityEvent m_MyEvent = new UnityEvent();
    public GameObject spawner;
    public int gg;
    int v;
    GameObject g;
    WWW www;

    // Use this for initialization
    void Start () {
        v = 0;
        pathimg = gameObject.GetComponent<savepanel>().pathing;
        another = pathimg;
        www = new WWW("file://" + another);

        gg = 1000;
        Button button = gameObject.GetComponent<Button>();
        button.onClick.AddListener(myAction);
        Debug.Log("ggg");
        
    }
	
	// Update is called once per frame
	void Update () {
        
        
        if (Input.GetKeyDown(KeyCode.Mouse0)& gg < 100)
        {
            
            Debug.Log("need spawn "+ another);
            Vector3 WorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
            
            Vector3 adj = new Vector3(WorldPoint.x, WorldPoint.y, 0);
            spawner.GetComponent<SpriteRenderer>().sprite =
              Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
            spawner.name = gameObject.name + v.ToString();
            GameObject el;
            //GameObject el = GameObject.Find(v.ToString() + "(Clone)");

            
                    
            Spawn(adj);
        }
    }

    public void Spawn(Vector3 position)
    {
        if (gg < 100)
        {
            
            
            Instantiate(spawner).transform.position = position;
            Debug.Log(v);
            g = GameObject.Find(gameObject.name + v.ToString() + "(Clone)");
            g.AddComponent<PolygonCollider2D>();
            g.GetComponent<PolygonCollider2D>().isTrigger = true;
            gg = 1000;
        }
    }

    public void Delete()
    {
        Destroy(g);
    }
    void myAction()
    {

        gg=spawnnum;
        v++;
        Debug.Log("ready to spawn");

    }
}
