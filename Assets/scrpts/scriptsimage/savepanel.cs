﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class savepanel : MonoBehaviour {
    [SerializeField]
    
    public loadpanel loadpo;
    public string pathing;
    public GameObject filemenu;
    public static int a = 0;
    private void Awake()
    {
        loadpo = FindObjectOfType<loadpanel>();
        loadpo.objects.Add(this);

    }

    private void Start()
    {

        filemenu = GameObject.Find("allScript");
        pathing = filemenu.GetComponent<FileSelectorImage>().path;

    }

    private void Update()
    {
        
    }

    public XElement GetXElement()
    {
        XAttribute n = new XAttribute("n", pathing);
        XAttribute v = new XAttribute("v", name);
        XElement element2 = new XElement("path", n,v);
        return element2;
    }


}
