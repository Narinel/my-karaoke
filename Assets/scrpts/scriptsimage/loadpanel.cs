﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;
using System.IO;
using UnityEngine.UI;
using System;
using UnityScript.Steps;

public class loadpanel : MonoBehaviour {
    public List<savepanel> objects = new List<savepanel>();
    savepanel ggg;
    public static string path;
    string pathing;
    GameObject filemenu;
    GameObject but;
    public GameObject knopka;
    public GameObject file;
    public GameObject preroll;


    // Use this for initialization
    void Start () {
        path = Application.persistentDataPath + "panelset.xml";
    }
	
	// Update is called once per frame
	void Update () {
        
        

    }

    public void Save()
    {
        XElement root = new XElement("root");
       

        foreach (savepanel obj in objects)
        {
            root.Add(obj.GetXElement());

        }
        XDocument saveDoc = new XDocument(root);

        File.WriteAllText(path, saveDoc.ToString());
        Debug.Log(path);
    }

    public void Load()
    {
        XElement root = null;
        Debug.Log("try load :" + path);

        if (!File.Exists(path))
        {
            Debug.Log("Save data not found");
            if (File.Exists(Application.persistentDataPath + "/panelset.xml")) ;

            root = XDocument.Parse(File.ReadAllText(Application.persistentDataPath + "/panelset.xml")).Element("root");
        }
        else
        {
            root = XDocument.Parse(File.ReadAllText(path)).Element("root");
        }

        if (root == null)
        {
            Debug.Log("failed");
            return;
        }

        GenerateScene(root);
    }

    private void GenerateScene(XElement root)
    {
        foreach (XElement instance in root.Elements("path"))
        {
            string a, b;
            int g;
            a = string.Copy(instance.Attribute("v").Value);
            b = string.Copy(instance.Attribute("n").Value);
            GameObject canvas = GameObject.Find("Image");
            but = Instantiate(preroll);
            but.gameObject.name = a;
            but.transform.SetParent(canvas.transform);
            but.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            but.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            knopka.transform.SetSiblingIndex(1000);
            WWW www = new WWW("file://" + b);
            but.GetComponent<Image>().sprite =
               Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
            g = Int32.Parse(a)  ;
            GetComponent<FileSelectorImage>().a = g;
            GameObject smart = GameObject.Find(g.ToString());
            smart.GetComponent<spawn>().spawnnum = g;
            smart.GetComponent<spawn>().pathimg = b;
        }
    }
}
