﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newopen : MonoBehaviour {

    public GameObject player;
    public GameObject but;
    public GameObject but2;
    public AudioSource aud;

    public float a = 220;



    public void plug()
    {
        
        player.gameObject.GetComponent<playercontrol>().speed = a;
        but.SetActive(false);
        but2.SetActive(true);
        a = 0;
        
        aud.Play();
    }

    public void plug2()
    {
        
        player.gameObject.GetComponent<playercontrol>().speed = a;
        but.SetActive(true);
        but2.SetActive(false);
        a = 220;
        
        aud.Pause();
    }
}
