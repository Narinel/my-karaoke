﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;
using System.IO;
using UnityEngine.UI;
using System;

public class savepoint : MonoBehaviour {

    public List<saveableobject> objects = new List<saveableobject>();
    public InputField xt;
    private string xv;
    saveableobject ggg;
    public static string path ;
    public string puth;
    public GameObject button;

    private void Awake()
    {

       // path = Application.persistentDataPath +  xv;//"/testsave.xml";
    }

    private void Start()
    {
        
    }

    private void Update()
    {
        
        // Debug.Log("/"+xt.text+".xml");
        xv = "/" + xt.text + ".xml";
        path = Application.persistentDataPath + xv;
        //if (Input.GetKeyDown(KeyCode.KeypadEnter)) Save();
        //if (Input.GetKeyDown(KeyCode.Backspace)) Load();
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            button.SetActive(true);
        }
    }
    


    public void Save()
    {
        Debug.Log(xv);
        XElement root = new XElement("root");
        puth = saveableobject.practic2;
        puth = Radio.river;
        XAttribute n = new XAttribute("n", puth);
        root.Add(new XElement("path", n));
        
        foreach (saveableobject obj in objects)
        {
            root.Add(obj.GetElement());
            
            //root.Add(obj.GetXElement());

        }
        


        XDocument saveDoc = new XDocument(root);
        
        File.WriteAllText(path, saveDoc.ToString());
        Debug.Log(path);
    }

    public void Load()
    {
        XElement root = null;
        Debug.Log("try load :" + path);

        if (!File.Exists(path))
        {
            Debug.Log("Save data not found");
            if (File.Exists(Application.persistentDataPath + "/level.xml")) ;

            root = XDocument.Parse(File.ReadAllText(Application.persistentDataPath + "/level.xml")).Element("root");
        }
        else
        {
            root = XDocument.Parse(File.ReadAllText(path)).Element("root");
        }

        if (root == null)
        {   
            Debug.Log("failed");
            return;
        }

        GenerateScene(root);
    }

    private void GenerateScene(XElement root)
    {
        
        foreach(XElement instance in root.Elements("instance"))
        {
            Debug.Log("generateScene");
            Vector3 position = Vector3.zero;

            position.x = float.Parse(instance.Attribute("x").Value);
            position.y = float.Parse(instance.Attribute("y").Value);
            position.z = float.Parse(instance.Attribute("z").Value);
            
            Instantiate(Resources.Load<GameObject>(instance.Value), position, Quaternion.identity) ;
            
        }
    }
}
