﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class levelchange : MonoBehaviour {
    public List<string> filespath;
    public List<string> filesname;
    public List<string> files;
    public List<string> files2;
    public GameObject prefab;
    GameObject content;
    public RectTransform contents;
    public static string d;
    public static string v;
    int a = 6;
    GameObject preload;

    private void Start()
    {
        Debug.Log(Application.persistentDataPath);
        filespath = Directory.GetFiles(Application.persistentDataPath).ToList();
        filesname = filespath;
        foreach (var s in filespath)
        {
            
            var ger = s.Replace(@"\", "/");
            
            files2.Add(ger);
            Debug.Log(a);
        }

        foreach (var file in filesname)
        {
            int pos = file.LastIndexOf(@"\");
            string a = file.Substring(pos + 1);
            string b = a;
            int pos2 = b.LastIndexOf(".");
            d = b.Substring(0, pos2);
            Debug.Log(d);
            files.Add(d);

        }
        for (int i = 0; i < files.Count; i++)
        {
            

            string path = Application.persistentDataPath + files[i];
           // Debug.Log(path);
        }


    }
    // Use this for initialization
    public void lex () {

       

        /* for (int i = 0; i < files.Count; i++)
        {
            Debug.Log(i);
            GameObject instance = Instantiate(prefab) as GameObject;
            instance.transform.SetParent(content.transform, false);
        }*/


    }

    // Update is called once per frame
    void Update () {
       // Debug.Log(v);
        dondestroy.b = v;
    }

    public void UpdateItem()
    {
        content = GameObject.Find("content");
        StartCoroutine(GetItems(filesname.Count, results => OnResivedModels(results)));
        
    }
    void OnResivedModels(TestItemModel[] models)
    {
        foreach (Transform child in contents)
        {
            Destroy(child.gameObject);
        }

        foreach(var model in models)
        {
            var instance = Instantiate(prefab) as GameObject;
            instance.transform.SetParent(content.transform, false);
            InitializeItemView(instance, model);
            lex();
        }
    }

    void InitializeItemView(GameObject viewGameObject, TestItemModel model)
    {
        TestItemView view = new TestItemView(viewGameObject.transform);
        view.titleText.text = model.title;
        view.clickButton.GetComponentInChildren<Text>().text = model.buttonText;
        view.clickButton.onClick.AddListener(
            () =>
            {
                v = view.titleText.text;
                Debug.Log(v);
                dondestroy.b = v;
               
              //  Debug.Log(view.titleText.text + " is clicked!");
            }
        );

    }

    IEnumerator GetItems(int count, System.Action<TestItemModel[]> callback)
    {
        yield return new WaitForSeconds(0.5f);
        var results = new TestItemModel[count];
        for (int i = 0; i< count; i++)
        {
            results[i] = new TestItemModel();
            results[i].title = files2[i];
            results[i].buttonText = files[i];
        }
        callback(results);
    }
    public class TestItemView
    {
        public Text titleText;
        public Button clickButton;

        public TestItemView(Transform rootView)
        {
            titleText = rootView.Find("TitleText").GetComponent<Text>();
            clickButton = rootView.Find("ClickButton").GetComponent<Button>();
        }
    }

    public class TestItemModel
    {
        public string title;
        public string buttonText;
    }
}


