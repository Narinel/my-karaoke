﻿using UnityEngine;
using UnityEngine.UI; //for accessing Sliders and Dropdown
using System.Collections.Generic; // So we can use List<>

[RequireComponent(typeof(AudioSource))]
public class MicrophoneInput : MonoBehaviour {
	public float minThreshold = 0;
	public float frequency = 0.0f;
    public float sensetivity;
    public float loudness = 0;
    public int audioSampleRate = 44100;
	public string microphone;
	public FFTWindow fftWindow;
	public Dropdown micDropdown;
	public Slider thresholdSlider;
    public Rigidbody2D playera;

    public List<string> options = new List<string>();
	private int samples = 8192; 
	private AudioSource audioSource;

	void Start() {
                //get components you'll need
        audioSource = GetComponent<AudioSource> ();

		// get all available microphones
		foreach (string device in Microphone.devices) {
			if (microphone == null) {
				//set default mic to first mic found.
				microphone = device;
			}
			options.Add(device);
		}
        microphone = options[PlayerPrefsManager.GetMicrophone()];
		minThreshold = PlayerPrefsManager.GetThreshold ();

		//add mics to dropdown
		micDropdown.AddOptions(options);
		micDropdown.onValueChanged.AddListener(delegate {
			micDropdownValueChangedHandler(micDropdown);
		});

		thresholdSlider.onValueChanged.AddListener(delegate {
			thresholdValueChangedHandler(thresholdSlider);
		});
		//initialize input with default mic
		UpdateMicrophone ();
	}

    void Update()
    {
        loudness = GetAveragedVolume() * sensetivity;
        if (loudness > 10)
            playera.AddForce(new Vector2(0, 430), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 50);
        if (loudness > 15)
            playera.AddForce(new Vector2(0, 470), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 90);
        if (loudness > 20)
            playera.AddForce(new Vector2(0, 490), ForceMode2D.Impulse);
        // playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 120);
        if (loudness > 25)
            playera.AddForce(new Vector2(0, 510), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 150);
        if (loudness > 35)
            playera.AddForce(new Vector2(0, 550), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 200);
        if (loudness > 45)
            playera.AddForce(new Vector2(0, 620), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 300);
        if (loudness > 50)
            playera.AddForce(new Vector2(0, 720), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 350);
        if (loudness > 53)
            playera.AddForce(new Vector2(0, 800), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 400);
        if (loudness > 55)
            playera.AddForce(new Vector2(0, 900), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 500);
        if (loudness > 59)
            playera.AddForce(new Vector2(0, 1050), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 550);
        if (loudness > 62)
            playera.AddForce(new Vector2(0, 1150), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 650);
        if (loudness > 65)
            playera.AddForce(new Vector2(0, 1300), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 800);
        if (loudness > 68)
            playera.AddForce(new Vector2(0, 1400), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 900);
        if (loudness > 72)
            playera.AddForce(new Vector2(0, 1500), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 1000);
        if (loudness > 75)
            playera.AddForce(new Vector2(0, 1700), ForceMode2D.Impulse);
        //playera.GetComponent<Rigidbody2D>().velocity = new Vector2(playera.GetComponent<Rigidbody2D>().velocity.x, 1200);

    }

    void UpdateMicrophone(){
		audioSource.Stop(); 
		//Start recording to audioclip from the mic
		audioSource.clip = Microphone.Start(microphone, true, 10, audioSampleRate);
		audioSource.loop = true; 
		// Mute the sound with an Audio Mixer group becuase we don't want the player to hear it
		Debug.Log(Microphone.IsRecording(microphone).ToString());

		if (Microphone.IsRecording (microphone)) { //check that the mic is recording, otherwise you'll get stuck in an infinite loop waiting for it to start
			while (!(Microphone.GetPosition (microphone) > 0)) {
			} // Wait until the recording has started. 
		
			Debug.Log ("recording started with " + microphone);

			// Start playing the audio source
			audioSource.Play (); 
		} else {
			//microphone doesn't work for some reason

			Debug.Log (microphone + " doesn't work!");
		}
	}


	public void micDropdownValueChangedHandler(Dropdown mic){
		microphone = options[mic.value];
		UpdateMicrophone ();
	}

	public void thresholdValueChangedHandler(Slider thresholdSlider){
		minThreshold = thresholdSlider.value;
	}
	
	public float GetAveragedVolume()
	{ 
		float[] data = new float[256];
		float a = 0;
		audioSource.GetOutputData(data,0);
		foreach(float s in data)
		{
			a += Mathf.Abs(s);
		}
		return a/256;
	}
	
	public float GetFundamentalFrequency()
	{
		float fundamentalFrequency = 0.0f;
		float[] data = new float[samples];
		audioSource.GetSpectrumData(data,0,fftWindow);
		float s = 0.0f;
		int i = 0;
		for (int j = 1; j < samples; j++)
		{
			if(data[j] > minThreshold) // volumn must meet minimum threshold
			{
				if ( s < data[j] )
				{
					s = data[j];
					i = j;
				}
			}
		}
		fundamentalFrequency = i * audioSampleRate / samples;
		frequency = fundamentalFrequency;
		return fundamentalFrequency;
	}
}