﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class loadmylevel2 : MonoBehaviour
{

    public string path;
    public GameObject[] texto;
    public Text text;
    public static string vs;
    public string sv;
    public string puth;
    private string xv;
    public List<saveableobject> objects = new List<saveableobject>();
    public InputField xt;


    private void Start()
    {
        GameObject canvas = GameObject.Find("textrender");
        path = dondestroy.b;
        
        Load();
        // texto = GameObject.FindWithTag("text");
        texto = GameObject.FindGameObjectsWithTag("text");
        foreach (var tex in texto)
        {

            tex.transform.SetParent(canvas.transform);

        }
    }
    private void Update()
    {
        /* xv = "/" + xt.text + ".xml";
         path = Application.persistentDataPath + xv;*/
    }

    public void Save()
    {
        Debug.Log(xv);
        XElement root = new XElement("root");
        puth = saveableobject.practic2;
        XAttribute n = new XAttribute("n", puth);
        root.Add(new XElement("path", n));

        foreach (saveableobject obj in objects)
        {
            root.Add(obj.GetElement());

            //root.Add(obj.GetXElement());

        }



        XDocument saveDoc = new XDocument(root);

        File.WriteAllText(path, saveDoc.ToString());
        Debug.Log(path);
    }

    public void Load()
    {
        XElement root = null;
        Debug.Log("try load :" + path);

        if (!File.Exists(path))
        {
            Debug.Log("Save data not found");
            if (File.Exists(Application.persistentDataPath + "/level.xml")) ;

            root = XDocument.Parse(File.ReadAllText(Application.persistentDataPath + "/level.xml")).Element("root");
        }
        else
        {
            root = XDocument.Parse(File.ReadAllText(path)).Element("root");
        }

        if (root == null)
        {
            Debug.Log("failed");
            return;
        }

        GenerateScene(root);
    }

    private void GenerateScene(XElement root)
    {

        foreach (XElement instance in root.Elements("instance"))
        {
            Debug.Log("generateScene");
            Vector3 position = Vector3.zero;

            position.x = float.Parse(instance.Attribute("x").Value);
            position.y = float.Parse(instance.Attribute("y").Value);
            position.z = float.Parse(instance.Attribute("z").Value);
            text.text = string.Copy(instance.Attribute("t").Value);


            Instantiate(Resources.Load<GameObject>(instance.Value), position, Quaternion.identity);

        }
        foreach (XElement instance in root.Elements("path"))
        {
            vs = string.Copy(instance.Attribute("n").Value);
            sv = vs;
        }
    }
}