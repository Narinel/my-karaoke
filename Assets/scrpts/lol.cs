﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lol : MonoBehaviour {
    public Slider sensitivitySlider;
    

    void Start () {
        sensitivitySlider.value = PlayerPrefsManager.GetSensitivity();
    }

    public void SaveAndExit()
    {
        PlayerPrefsManager.SetSensitivity(sensitivitySlider.value);
    }
}
