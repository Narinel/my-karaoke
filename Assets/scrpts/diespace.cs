﻿
using UnityEngine;
using UnityEngine.Audio;

public class diespace : MonoBehaviour {

    private GameObject respawn;
    public AudioSource rudio;
    private GameObject player;

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        respawn = GameObject.FindWithTag("Respawn");
        rudio = GameObject.FindGameObjectWithTag("rudio").GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player");
        
        if (other.tag == "Player")
        {
            other.transform.position = respawn.transform.position;
            
            rudio.Stop();
        }

        
    }
   
}
